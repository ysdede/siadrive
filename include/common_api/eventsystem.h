#ifndef _EVENTSYSTEM_H
#define _EVENTSYSTEM_H
#include <siacommon.h>
#include <condition_variable>
#include <siaapi.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)
enum class EventLevel {
  Error,
  Warn,
  Normal,
  Debug,
  Verbose
};

EventLevel SIADRIVE_EXPORTABLE EventLevelFromString(const SString &eventLevel);

class CEvent;
typedef std::shared_ptr<CEvent> CEventPtr;

INTERFACE_CLASS_BEGIN(IEventForwarder)
  METHOD(void ForwardEvent(const json & event))
INTERFACE_CLASS_END(IEventForwarder)

// Singleton
class SIADRIVE_EXPORTABLE CEventSystem {
    friend class IEventRegister;

  public:
    NON_COPYABLE_ASSIGNABLE(CEventSystem)

  private:
    CEventSystem();
    ~CEventSystem();

  public:
    static CEventSystem EventSystem;

  private:
    bool _stopRequested = false;
    std::deque<CEventPtr> _eventQueue;
    std::deque<std::function<void(const CEvent &)>> _eventConsumers;
    std::deque<std::function<bool(const CEvent &)>> _oneShotEventConsumers;
    std::mutex _eventMutex;
    std::mutex _stopMutex;
    std::condition_variable _stopEvent;
    std::unique_ptr<std::thread> _processThread;
    std::mutex _oneShotMutex;
    std::unordered_map<SString, std::function<CEventPtr(const json& j)>> _registerMap;
    std::vector<IEventForwarder*> _forwarderMap;

  private:
    CEventPtr CreateJsonEvent(const json& j);
    void ProcessEvents();
    void RegisterJsonFactory(const SString& eventName, std::function<CEventPtr(const json& j)> factory);

  public:
    void AddEventConsumer(std::function<void(const CEvent &)> consumer);
    void AddEventForwarder(IEventForwarder* eventForwarder);
    void AddOneShotEventConsumer(std::function<bool(const CEvent &)> consumer);
    void NotifyEvent(CEventPtr eventData);
    void NotifyJsonEvent(const json& j);
    void Start();
    void Stop();
};


INTERFACE_CLASS_BEGIN(IEventRegister)
  protected:
    inline void RegisterJsonFactory(const SString& eventName, std::function<CEventPtr(const json& j)> factory) {
      CEventSystem::EventSystem.RegisterJsonFactory(eventName, factory);
    }
INTERFACE_CLASS_END(IEventRegister)

template <typename T>
class EventRegister :
  public virtual IEventRegister {
  public:
    EventRegister(const SString& eventName) {
      RegisterJsonFactory(eventName, [](const json& j) {
        auto ret = std::shared_ptr<T>(new T());
        ret->LoadData(j);
        return ret;
      });
    }
};

class SIADRIVE_EXPORTABLE CEvent {
  public:
    CEvent(const EventLevel &eventLevel) :
      _eventLevel(eventLevel) {
    }

    virtual ~CEvent() {
    }

  private:
    const EventLevel _eventLevel;

  protected:
    virtual void LoadData(const json& t) = 0;

  public:
    const EventLevel &GetEventLevel() const {
      return _eventLevel;
    }

    virtual SString GetSingleLineMessage() const = 0;
    virtual std::shared_ptr<CEvent> Clone() const = 0;
    virtual SString GetEventName() const = 0;
    virtual json GetEventJson() const = 0;
};

#define BEGIN_EVENT(name) \
class name : public virtual CEvent { \
    friend class EventRegister<name>;\
  private: \
    static EventRegister<name> _eventRegister;\
  public: \
    virtual SString GetEventName() const override { \
      return #name; \
    }

#define END_EVENT(name) };\
EventRegister<name> name::_eventRegister(#name);

CEventPtr SIADRIVE_EXPORTABLE CreateSystemCriticalEvent(const SString &reason);
CEventPtr SIADRIVE_EXPORTABLE CreateFileClosedEvent(const SString &siaPath, const FilePath &filePath, const bool &wasChanged);
CEventPtr SIADRIVE_EXPORTABLE CreateDownloadToCacheBeginEvent(const SString &siaPath, const FilePath &filePath, const FilePath &tempFilePath);
CEventPtr SIADRIVE_EXPORTABLE CreateDownloadToCacheEndEvent(const SString &siaPath, const FilePath &filePath, const FilePath &tempFilePath, const SiaApiError &result);
CEventPtr SIADRIVE_EXPORTABLE CreateCacheFileAddedEvent(const SString &siaPath, const FilePath &filePath, const bool &succeeded, const SString &reason = "");
CEventPtr SIADRIVE_EXPORTABLE CreateFileUnrecoverableEvent(const SString &siaPath, const FilePath &filePath);
CEventPtr SIADRIVE_EXPORTABLE CreateMountEndedEvent(const SString &mountLocation, const std::int32_t &result);
CEventPtr SIADRIVE_EXPORTABLE CreateMountedEvent(const SString &mountLocation);
CEventPtr SIADRIVE_EXPORTABLE CreateUnMountedEvent(const SString &mountLocation);
CEventPtr SIADRIVE_EXPORTABLE CreateCacheEvictionEvent(const SString &siaPath, const FilePath &filePath, const bool& cacheSizeEviction, const double& redundancy);
CEventPtr SIADRIVE_EXPORTABLE CreateCacheFullEvent(const SString &mountLocation);
CEventPtr SIADRIVE_EXPORTABLE CreateSiaOnlineEvent();
CEventPtr SIADRIVE_EXPORTABLE CreateSiaOfflineEvent()
;
NS_END(2)
#endif //_EVENTSYSTEM_H