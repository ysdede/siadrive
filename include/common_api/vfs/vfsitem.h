#ifndef SIADRIVE_FILEENTRY_H
#define SIADRIVE_FILEENTRY_H
#include <siacommon.h>
#include <vfs/vfscommon.h>
#include <vfs/ivfsitem.h>
#include <vfs/itemlock.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)

template<typename Handle, typename AccessData, typename OpenData>
class CVFSItem :
  public virtual IVFSItem<Handle, AccessData, OpenData> {
  public:
    typedef Handle ItemHandle;
    typedef OpenData ItemOpenData;
    typedef AccessData ItemAccessData;

  private:
    typedef std::function<bool(CVFSItem *item)> CacheRequester;
    typedef std::function<bool(CVFSItem *item)> CloseCallback;
    typedef std::function<void(CVFSItem *item)> ModifiedCallback;
    typedef std::function<void(CVFSItem *item)> RefreshCallback;

  public:
    explicit CVFSItem(const std::uint64_t &id, RefreshCallback refreshCallback, CloseCallback closeCallback, ModifiedCallback modifiedCallback, CacheRequester cacheRequester) :
      _id(id),
      _refreshCallback(refreshCallback),
      _closeCallback(closeCallback),
      _modifiedCallback(modifiedCallback) {
      Refresh();
    }

    virtual ~CVFSItem() {
    }

  private:
    const std::uint64_t _id;
    std::uint64_t _iid;
    RefreshCallback _refreshCallback;
    CloseCallback _closeCallback;
    ModifiedCallback _modifiedCallback;
    CacheRequester _cacheRequester;
    Handle _handle;
    SString _path;
    SString _sourcePath;
    VFSItemStatus _status;
    bool _isCached;
    AccessData _accessData;
    OpenData _openData;
    bool _isDirectory;
    std::uint64_t _date;
    std::int64_t _fileSize;

  private:
    bool CreateStubIfNotFound() {
      return false;
    }

    inline void Refresh() {
      _refreshCallback(this);
    }

  public:
    inline AccessData GetAccessData() const override {
      return _accessData;
    }

    inline void SetAccessData(const AccessData& accessData) {
      _accessData = accessData;
    }
    
    inline std::uint64_t GetDate() const override {
      return _date;
    }

    inline void SetDate(const std::uint64_t& date) {
      _date = date;
    }

    inline Handle GetHandle() const override {
      return _handle;
    }

    inline void SetHandle(Handle handle) {
      _handle = handle;
    }

    inline std::int64_t GetSize() const override {
      return _fileSize;
    }

    inline void SetSize(const std::int64_t& fileSize) {
      _fileSize = fileSize;
    }

    inline std::uint64_t GetId() const override {
      return _id;
    }

    inline std::uint64_t GetItemId() const override {
      return _iid;
    }

    inline void SetItemId(const std::uint64_t& iid) {
      _iid = iid;
    }

    inline OpenData GetOpenData() const override {
      return _openData;
    }

    inline void SetOpenData(const OpenData& openData) {
      _openData = openData;
    }

    inline SString GetPath() const override {
      return _path;
    }

    inline void SetPath(const SString& path) {
      _path = path;
    }

    inline SString GetSourcePath() const override {
      return _sourcePath;
    }

    inline void SetSourcePath(const SString& sourcePath) {
      _sourcePath = sourcePath;
    }

    inline VFSItemStatus GetStatus() const {
      return _status;
    }

    inline void SetStatus(const VFSItemStatus &status) {
      _status = status;
    }

    inline bool IsCached() const override {
      return _isCached;
    }

    inline void SetIsCached(const bool& cached) {
      _isCached = cached;
    }

    inline bool IsDirectory() const override {
      return _isDirectory;
    }

    inline void SetIsDirectory(const bool& directory) {
      _isDirectory = directory;
    }

    inline void Close() override {
      ItemLock lock(GetPath());
      _closeCallback(this);
    }

    bool Read(char *data, const std::size_t &dataSize, const std::int64_t &offset, std::size_t &bytesRead) override {
      return false;
    }

    bool ReadOperation(std::function<bool(Handle handle)> operation, const bool& stubAllowed) override {
      bool ret = false;
      if (not IsDirectory()) {
        if (stubAllowed) {
          ret = CreateStubIfNotFound();
        } else if (not IsCached() || not FilePath(GetSourcePath()).IsFile()) {
          ret = _cacheRequester(this);
        }

        ret = ret && operation(GetHandle());
      }
      return ret;
    }

    bool Truncate(const std::uint64_t& size) override {
      return false;
    }

    bool Write(const char *data, const std::size_t &dataSize, const std::int64_t &offset, std::size_t &bytesWritten) override {
      bool ret = false;
      if (not IsDirectory()) {
        if (not IsCached() || not FilePath(GetSourcePath()).IsFile()) {
          ret = _cacheRequester(this);
        }
#ifdef _WIN32
        ::WriteItem();
        {
#else
        auto err = pwrite(GetHandle(), data, dataSize, offset);
        if (err == -1) {
          bytesWritten = 0;
          CEventSystem::EventSystem.NotifyEvent(CreateVFSErrorEvent(__FUNCTION__, SString::FromInt32(errno)));
        } else {
          bytesWritten = err;
#endif
          if (GetStatus() != VFSItemStatus::Modified) {
            _modifiedCallback(this);
          }
          ret = true;
        }
      }
      return ret;
    }

    bool WriteOperation(std::function<bool(Handle handle)> operation) override {
      bool ret = false;
      if (not IsDirectory()) {
        if (not IsCached() || not FilePath(GetSourcePath()).IsFile()) {
          ret = _cacheRequester(this);
        }

        ret = ret && operation(GetHandle());
        if (ret && (GetStatus() != VFSItemStatus::Modified)) {
          _modifiedCallback(this);
        }
      }

      return ret;
    }
};

NS_END(2)
#endif //SIADRIVE_FILEENTRY_H
