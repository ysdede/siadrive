#ifndef _LISTENERBASE_H
#define _LISTENERBASE_H
#include <siacommon.h>
#include <siaapi.h>
#include <cpprest/http_listener.h>

using namespace web::http;
using namespace web::http::experimental::listener;

NS_BEGIN(Sia)
NS_BEGIN(Api)
class CSiaDriveConfig;

class CListenerBase {
  public:
    enum class Protocol {
        Http,
        Https
    };
    static SString ProtocolToString(const Protocol& protocol) {
      switch(protocol) {
      case Protocol::Http:
        return "http";
      case Protocol::Https:
        return "https";
      }
    }
    static Protocol StringToProtocol(const SString& protocol) {
      if (protocol == "https") {
        return Protocol::Https;
      }

      return Protocol::Http;
    }

    enum class ListenerInterface {
        Private,
        Public
    };

  public:
    explicit CListenerBase(std::shared_ptr<CSiaApi> siaApi, const Protocol& protocol, const ListenerInterface& listenerInterface, const std::uint16_t& port);

  public:
    virtual ~CListenerBase();

  private:
    std::shared_ptr<CSiaApi> _siaApi;
    std::vector<std::unique_ptr<http_listener>> _listenerList;

  protected:
    virtual void ConfigureListener(http_listener* listener) = 0;

    inline CSiaApi& GetSiaApi() {
      return *_siaApi;
    }

    inline const CSiaApi& GetSiaApi() const {
      return *_siaApi;
    }

    inline CSiaDriveConfig& GetSiaDriveConfig() {
      return *_siaApi->GetSiaDriveConfig();
    }

    inline const CSiaDriveConfig& GetSiaDriveConfig() const {
      return *_siaApi->GetSiaDriveConfig();
    }

  public:
    void Start();
    void Stop();
};
NS_END(2)

#endif //_LISTENERBASE_H
