#ifndef _PUBLICLISTENER_H
#define _PUBLICLISTENER_H
#include <siacommon.h>
#include <listenerbase.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)
class CSiaApi;
class CSiaDriveConfig;

class CPublicListener :
  public CListenerBase {
  public:
    CPublicListener(std::shared_ptr<CSiaApi> siaApi);

  public:
    virtual ~CPublicListener() {}

  protected:
    void ConfigureListener(http_listener* listener) override;
};
NS_END(2)

#endif //_PUBLICLISTENER_H
