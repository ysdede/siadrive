# SiaDrive
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/87720107be1b41229dc1fdcbd389e9cd)](https://www.codacy.com/app/scott.e.graves/SiaDrive?utm_source=siaextensions@bitbucket.org&amp;utm_medium=referral&amp;utm_content=siaextensions/siadrive&amp;utm_campaign=Badge_Grade)

SiaDrive is a user-mode filesystem/storage driver for Windows 7/8/8.1/10, a FUSE filesystem implementation for Linux/OSX. Mount Sia to any available drive letter on Windows or any directory location on Linux/OSX.

# Desktop GUI
[SiaDriveUI](https://bitbucket.org/siaextensions/siadriveui)

# Downloads
* COMING SOON

# Build Instructions
[BUILDING](https://bitbucket.org/siaextensions/siadrive/raw/master/BUILDING)

# Linux Configuration Details
- Cache location (~/.config/siadrive/cache)
- Siad data location (~/.config/siadrive/data)
- Configuration file (~/.config/siadrive/siadriveconfig.json)
- Log location (~/.config/siadrive/logs/siadrive.log)

# Mac Configuration Details
- Cache location (~/Application Support/siadrive/cache)
- Siad data location (~/Application Support/siadrive/data)
- Configuration file (~/Application Support/siadrive/siadriveconfig.json)
- Log location (~/Application Support/siadrive/logs/siadrive.log)

# Windows Configuration Details
- Cache location (%localappdata%\SiaDrive\cache)
- Siad data location (%localappdata%\SiaDrive\data)
- Configuration file (%localappdata%\SiaDrive\siadriveconfig.json)
- Log location (%localappdata%\SiaDrive\logs\siadrive.log)

# Important Notes
- It is recommended to use the bundled Sia version; however, SiaDrive will work with existing Sia-UI or siad instances.
- The wallet needs to be unlocked and running at all times, especially near contrtact renewal period. It's best to run SiaDrive on a computer that is powered-on most of the day.
- It's best to store files that do not change frequently. This is not a requirement but it can become a bit costly. Modified files result in a delete and re-upload to Sia. Future versions of Sia should make this less of an issue as deleted space will be reclaimed on hosts.

# Bugs and Feature Requests
- [Submit Here](https://bitbucket.org/siaextensions/siadrive/issues?status=new&status=open)
- [Email](mailto:sia.extensions@gmail.com)

# Credits
- [Chromium Embedded Framework](https://bitbucket.org/chromiumembedded/cef)
- [CryptoPP](https://www.cryptopp.com/)
- [Dokany](https://github.com/dokan-dev/dokany)
- [JSON for Modern C++](https://github.com/nlohmann/json)
- [keyutils](https://git.kernel.org/pub/scm/linux/kernel/git/dhowells/keyutils.git)
- [PicoSHA2](https://github.com/okdshin/PicoSHA2)
- [Sia](https://sia.tech/)
- [SQLiteCpp](https://github.com/SRombauts/SQLiteCpp)
- [ThreadPool](https://github.com/progschj/ThreadPool)
- [ttmath](http://www.ttmath.org/)

# Tips
- BTC: 18FhNwu4vgun3AZxUav9ncVCCyxfH3H3GN
- XMR: 44TLBj32YZaiAcx5kXkM1Nf3VUQTkqYQbbZdKAd3php1M9h8TViANguBhRQnCAZeDa3yyrJscGMX9Hcg4nyxbkS1NamoKLP
-  SC: 468fec9d2e903e6c2830e01c63eb11fb6b804e5157b3cc9b613f8f2e3adc77ecc814dfe34b4f