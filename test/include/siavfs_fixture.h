#ifndef SIADRIVE_SIAVFS_FIXTURE_H
#define SIADRIVE_SIAVFS_FIXTURE_H

#include "unittestcommon.h"
#include <siavfs.h>
#include "mocktransfermanager.h"

NS_BEGIN(Sia)
NS_BEGIN(Api)

typedef CVirtualFileSystem<VFSItemStore> TestVFS;

class SiaVfsTest :
  public ::testing::Test {
  public:
    virtual ~SiaVfsTest() {
    }

  protected:
    CMockTransferManager _tm;
    std::unique_ptr<TestVFS> vfs;

  protected:
    virtual void SetUp() override {
      FilePath("./test.db").DeleteAsFile();
      vfs = std::make_unique<TestVFS>(&_tm, "./temp_cache", VFS_ACCESS_CHECKER, VFS_CREATOR, VFS_CLOSER, (SString)FilePath("./test.db").MakeAbsolute(), SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE);
      vfs->Upgrade();

      AccessData accessData{};
      accessData.Mode = 0750l;
      accessData.UID = getuid();
      accessData.GID = getgid();
      OpenData openData{};
      openData.Flags = O_CREAT|O_DIRECTORY;
      VirtualFileSystem::VFSItemPtr rootDir;
      const auto ec = vfs->OpenOrCreate("/", true, accessData, openData, rootDir);
      if (ec == VFSErrorCode::Success) {
        rootDir->Close();
      } else {
        throw StartupException("Failed to create root directory: " + SString::FromInt32(VFSUtils::TranslateVFSErrorCode(ec)));
      }
    }

    virtual void TearDown() override {
      vfs.reset();
      FilePath("./test.db").DeleteAsFile();
    }
};

NS_END(2)
#endif //SIADRIVE_SIAVFS_FIXTURE_H
