#include "unittestcommon.h"
#include "siaapi_fixture.h"

TEST_F(SiaApiTest, SiaWallet_StateWhenOffline) {
  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet"), _))
    .WillOnce(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  auto &wallet = siaApi->GetWallet();

  wallet.Refresh();
  EXPECT_FALSE(wallet.GetConnected());
  EXPECT_FALSE(wallet.GetCreated());
  EXPECT_FALSE(wallet.GetLocked());
  EXPECT_TRUE(wallet.GetReceiveAddress().IsNullOrEmpty());
}

TEST_F(SiaApiTest, SiaWallet_StateWhenNotCreated) {
  json result = ReadJson(FilePath("./data/wallet_not_created.json"));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet/address"), _))
    .WillOnce(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  siaApi->GetWallet().Refresh();

  EXPECT_TRUE(siaApi->GetWallet().GetConnected());
  EXPECT_TRUE(siaApi->GetWallet().GetLocked());
  EXPECT_FALSE(siaApi->GetWallet().GetCreated());
}

TEST_F(SiaApiTest, SiaWallet_StateWhenCreatedAndLocked) {
  json result = ReadJson(FilePath("./data/wallet_created_locked.json"));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet/address"), _))
    .WillOnce(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  siaApi->GetWallet().Refresh();

  EXPECT_TRUE(siaApi->GetWallet().GetConnected());
  EXPECT_TRUE(siaApi->GetWallet().GetLocked());
  EXPECT_TRUE(siaApi->GetWallet().GetCreated());
}

TEST_F(SiaApiTest, SiaWallet_StateWhenCreatedAndUnlocked) {
  json result = ReadJson(FilePath("./data/wallet_created_unlocked.json"));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet/address"), _))
    .WillOnce(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  siaApi->GetWallet().Refresh();

  EXPECT_TRUE(siaApi->GetWallet().GetConnected());
  EXPECT_FALSE(siaApi->GetWallet().GetLocked());
  EXPECT_TRUE(siaApi->GetWallet().GetCreated());
}

TEST_F(SiaApiTest, SiaWallet_AddressWhenCreatedAndUnlocked) {
  json result = ReadJson(FilePath("./data/wallet_created_unlocked.json"));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  result = ReadJson(FilePath("./data/wallet_receive_address.json"));
  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet/address"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  siaApi->GetWallet().Refresh();

  EXPECT_STREQ(siaApi->GetWallet().GetReceiveAddress().str()
                 .c_str(), SString("1234567890abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789ab")
                 .str().c_str());
}

TEST_F(SiaApiTest, SiaWallet_StateWhenTransitionedFromOnlineToOffline) {
  json result = ReadJson(FilePath("./data/wallet_created_unlocked.json"));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  result = ReadJson(FilePath("./data/wallet_receive_address.json"));
  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet/address"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  auto &wallet = siaApi->GetWallet();
  wallet.Refresh();

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet"), _)).WillOnce(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  wallet.Refresh();
  EXPECT_FALSE(wallet.GetConnected());
  EXPECT_FALSE(wallet.GetCreated());
  EXPECT_FALSE(wallet.GetLocked());
  EXPECT_TRUE(wallet.GetReceiveAddress().IsNullOrEmpty());
}

TEST_F(SiaApiTest, SiaWallet_CreateUsingSeedAsPassword) {
  json result = ReadJson(FilePath("./data/wallet_not_created.json"));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet/address"), _))
    .WillOnce(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  auto &wallet = siaApi->GetWallet();
  wallet.Refresh();

  json result2 = ReadJson(FilePath("./data/wallet_created_seed.json"));
  HttpParameters params = {{"dictionary", "english"}};
  EXPECT_CALL(*mockComm, Post(SStrEq("/wallet/init"), params, _))
    .WillOnce(DoAll(SetArgReferee<2>(result2), Return(SiaCommError(SiaCommErrorCode::Success))));

  SString seed;
  auto ret = wallet.Create("", SiaSeedLanguage::English, seed);

  EXPECT_TRUE(ApiSuccess(ret));
  EXPECT_STREQ(SString("hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello")
                 .str().c_str(), seed.str().c_str());
  EXPECT_TRUE(wallet.GetCreated());
  EXPECT_TRUE(wallet.GetLocked());
}

TEST_F(SiaApiTest, SiaWallet_CreateUsingPassword) {
  json result = ReadJson(FilePath("./data/wallet_not_created.json"));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet/address"), _))
    .WillOnce(Return(SiaCommError(SiaCommErrorCode::NoResponse)));

  auto &wallet = siaApi->GetWallet();
  wallet.Refresh();

  json result2 = ReadJson(FilePath("./data/wallet_created_seed.json"));
  HttpParameters params = {{L"dictionary",         "english"},
                           {L"encryptionpassword", "password"}};
  EXPECT_CALL(*mockComm, Post(SStrEq("/wallet/init"), params, _))
    .WillOnce(DoAll(SetArgReferee<2>(result2), Return(SiaCommError(SiaCommErrorCode::Success))));

  SString seed;
  auto ret = wallet.Create("password", SiaSeedLanguage::English, seed);

  EXPECT_TRUE(ApiSuccess(ret));
  EXPECT_STREQ(SString("hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello")
                 .str().c_str(), seed.str().c_str());
  EXPECT_TRUE(wallet.GetCreated());
  EXPECT_TRUE(wallet.GetLocked());
}

TEST_F(SiaApiTest, SiaWallet_ResetReceiveAddress) {
  json result = ReadJson(FilePath("./data/wallet_created_unlocked.json"));

  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  result = ReadJson(FilePath("./data/wallet_receive_address.json"));
  EXPECT_CALL(*mockComm, Get(SStrEq("/wallet/address"), _))
    .WillOnce(DoAll(SetArgReferee<1>(result), Return(SiaCommError(SiaCommErrorCode::Success))));

  siaApi->GetWallet().Refresh();
  siaApi->GetWallet().ResetReceiveAddress();

  EXPECT_TRUE(siaApi->GetWallet().GetReceiveAddress().IsNullOrEmpty());
}
