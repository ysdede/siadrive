#include <siaapi.h>
#include <regex>
#include <siadriveconfig.h>
#include <platform.h>
#include <filepath.h>
#include <eventsystem.h>

using namespace Sia::Api;

// Event Notifications
BEGIN_EVENT(SiadProcessLaunched)
public:
  SiadProcessLaunched(const SiaApiError &result) :
    CEvent(EventLevel::Debug),
    _result(result.GetReason()) {
  }

private:
  SiadProcessLaunched(const SString &result = "") :
    CEvent(EventLevel::Debug),
    _result(result) {
}

public:
  virtual ~SiadProcessLaunched() {
  }

private:
  SString _result;

protected:
  virtual void LoadData(const json& j) override {
    _result = j["result"].get<std::string>();    
  }

public:
  virtual SString GetSingleLineMessage() const override {
    return GetEventName() + "|" + _result;
  }

  virtual std::shared_ptr<CEvent> Clone() const override {
    return std::shared_ptr<CEvent>(new SiadProcessLaunched(_result));
  }

  virtual json GetEventJson() const override {
    return {{"event", GetEventName()},
            {"result", _result}};
  }
END_EVENT(SiadProcessLaunched)

CSiaApi::CSiaApi(std::shared_ptr<CSiaDriveConfig> siaDriveConfig, const bool& enableRefresh) :
  CSiaBase(siaDriveConfig),
  _asyncServerVersion(""),
  _wallet(new CSiaWallet(siaDriveConfig)),
  _renter(new CSiaRenter(siaDriveConfig)),
  _consensus(new CSiaConsensus(siaDriveConfig)),
  _hostDb(new CSiaHostDb(siaDriveConfig)) {
  if (enableRefresh) {
    _refreshThread= std::make_unique<CAutoThread>(siaDriveConfig, [this](std::shared_ptr<CSiaDriveConfig> sdc) {
      this->FullRefresh();
    });
  }
}

CSiaApi::~CSiaApi() {
  if (_refreshThread) {
    _refreshThread->StopAutoThread();
  }
  _asyncServerVersion = "";
}

void CSiaApi::FullRefresh() {
  this->Refresh();
  this->_consensus->Refresh();
  this->_renter->Refresh();
  this->_hostDb->Refresh();
  // Initialize last - wallet contains connection status
  this->_wallet->Refresh();
}

void CSiaApi::Refresh() {
  _asyncServerVersion = GetServerVersion();
}

SString CSiaApi::FormatToSiaPath(SString path) {
  if (path.Length()) {
    std::replace(path.begin(), path.end(), '\\', '/');
    if (path.BeginsWith("./")) {
      path = path.SubString(1);
    }
    std::regex r(SString::ActiveString("/+"));
    path = std::regex_replace(path.str(), r, SString::ActiveString("/"));
    while (path[0] != '/') {
      path = "/" + path;
    }
  } else {
    path = "/";
  }
  return path;
}

bool CSiaApi::GetOwnsSiad() const {
  return PlatformIsProcessRunningExactPath(FilePath("./sia/", SIAD_EXECUTABLE));
}

void CSiaApi::StartBackgroundRefresh() {
  _refreshThread->StartAutoThread();
}

void CSiaApi::StopBackgroundRefresh() {
  _refreshThread->StopAutoThread();
}

SiaApiError CSiaApi::StartSiad(const bool& force) {
  SiaApiError ret;
  if (GetSiaDriveConfig()->GetLaunchBundledSiad() || force) {
    if (GetServerVersion().IsNullOrEmpty()) {
      if (not PlatformLaunchBundledSiad(GetSiaDriveConfig())) {
        ret = { SiaApiErrorCode::SiadFailedToLaunch };
      }
    } else {
      ret = {SiaApiErrorCode::SiadAlreadyRunning};
    }
  } else {
    ret = {SiaApiErrorCode::SiadLaunchNotPermitted};
  }
  
  CEventSystem::EventSystem.NotifyEvent(std::make_shared<SiadProcessLaunched>(ret));

  return ret;
}

SiaApiError CSiaApi::StopSiad(const bool& force) {
  SiaApiError ret;
  if (GetOwnsSiad() || force) {
    json result;
    auto error = GetSiaDriveConfig()->GetSiaCommunicator()->Get("/daemon/stop", result);
    if (not ApiSuccess(error)) {
      ret = { SiaApiErrorCode::SiadFailedToStop, error.GetReason() };
    }
  } else {
    ret = { SiaApiErrorCode::SiadStopNotPermitted };
  }
  return ret;
}

SString CSiaApi::GetServerVersion() const {
  return GetSiaDriveConfig()->GetSiaCommunicator()->GetServerVersion();
}
