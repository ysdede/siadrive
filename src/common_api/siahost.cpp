#include <siaapi.h>

#include <utility>

using namespace Sia::Api;

/*
{
  // true if the host is accepting new contracts.
  "acceptingcontracts": true,

  // Maximum number of bytes that the host will allow to be requested by a
  // single download request.
  "maxdownloadbatchsize" : 17825792,

  // Maximum duration in blocks that a host will allow for a file contract.
  // The host commits to keeping files for the full duration under the
  // threat of facing a large penalty for losing or dropping data before
  // the duration is complete. The storage proof window of an incoming file
  // contract must end before the current height + maxduration.
  //
  // There is a block approximately every 10 minutes.
  // e.g. 1 day = 144 blocks
  "maxduration": 25920,

  // Maximum size in bytes of a single batch of file contract
  // revisions. Larger batch sizes allow for higher throughput as there is
  // significant communication overhead associated with performing a batch
  // upload.
  "maxrevisebatchsize": 17825792,

  // Remote address of the host. It can be an IPv4, IPv6, or hostname,
  // along with the port. IPv6 addresses are enclosed in square brackets.
  "netaddress" : "123.456.789.0:9982",

  // Unused storage capacity the host claims it has, in bytes.
  "remainingstorage" : 35000000000,

  // Smallest amount of data in bytes that can be uploaded or downloaded to
  // or from the host.
  "sectorsize" : 4194304,

  // Total amount of storage capacity the host claims it has, in bytes.
  "totalstorage" : 35000000000,

  // Address at which the host can be paid when forming file contracts.
  "unlockhash" : "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789ab",

  // A storage proof window is the number of blocks that the host has to
  // get a storage proof onto the blockchain. The window size is the
  // minimum size of window that the host will accept in a file contract.
  "windowsize" : 144,

  // Public key used to identify and verify hosts.
  "publickey" : {
  // Algorithm used for signing and verification. Typically "ed25519".
  "algorithm": "ed25519",

    // Key used to verify signed host messages.
    "key" : "RW50cm9weSBpc24ndCB3aGF0IGl0IHVzZWQgdG8gYmU="
  }
}
*/

CSiaApi::_CSiaHost::_CSiaHost(std::shared_ptr<CSiaDriveConfig> siaDriveConfig, const json &hostJson) :
  CSiaBase(std::move(siaDriveConfig)),
  _AcceptingContracts(hostJson["acceptingcontracts"].get<bool>()),
  _MaxDownloadBatchSize(hostJson["maxdownloadbatchsize"].get<std::uint64_t>()),
  _MaxDuration(hostJson["maxduration"].get<std::uint64_t>()),
  _MaxReviseBatchSize(hostJson["maxrevisebatchsize"].get<std::uint16_t>()),
  _NetAddress(hostJson["netaddress"].get<std::string>()),
  _RemainingStorage(hostJson["remainingstorage"].get<std::uint64_t>()),
  _SectorSize(hostJson["sectorsize"].get<std::uint64_t>()),
  _TotalStorage(hostJson["totalstorage"].get<std::uint64_t>()),
  _UnlockHash(hostJson["unlockhash"].get<std::string>()),
  _WindowSize(hostJson["windowsize"].get<std::uint64_t>()),
  _PublicKey({hostJson["publickey"]["algorithm"].get<std::string>(),
              hostJson["publickey"]["key"].get<std::string>()}) {
}

CSiaApi::_CSiaHost::~_CSiaHost() = default;
