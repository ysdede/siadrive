#include <autothread.h>
#include <siacurl.h>

#include <utility>

using namespace Sia::Api;
using namespace std::chrono_literals;

CAutoThread::CAutoThread(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) :
  CAutoThread(std::move(siaDriveConfig), nullptr) {
}

CAutoThread::CAutoThread(std::shared_ptr<CSiaDriveConfig> siaDriveConfig, std::function<void(std::shared_ptr<CSiaDriveConfig>)> autoThreadCallback) :
  _stopRequested(false),
  _siaDriveConfig(std::move(siaDriveConfig)),
  _AutoThreadCallback(std::move(autoThreadCallback)) {
}

CAutoThread::~CAutoThread() {
  CAutoThread::StopAutoThread();
}

void CAutoThread::AutoThreadCallback(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) {
  if (_AutoThreadCallback) {
    _AutoThreadCallback(std::move(siaDriveConfig));
  }
}

bool CAutoThread::GetStopRequested() const {
  return _stopRequested;
}

void CAutoThread::StartAutoThread() {
  std::lock_guard<std::mutex> l(_startStopMutex);
  if (not _thread) {
    _stopRequested = false;
    _thread.reset(new std::thread([this]() {
      do {
        AutoThreadCallback(_siaDriveConfig);
        std::unique_lock<std::mutex> l(_stopMutex);
        _stopEvent.wait_for(l, 5s);
      } while (not _stopRequested);
    }));
  }
}

void CAutoThread::StopAutoThread() {
  std::lock_guard<std::mutex> l(_startStopMutex);
  if (_thread) {
    {
      std::unique_lock<std::mutex> l2(_stopMutex);
      _stopRequested = true;
    }
    _stopEvent.notify_all();
    _thread->join();
    _thread.reset(nullptr);
    _stopRequested = false;
  }
}

bool CAutoThread::IsRunning() const {
  return (_thread != nullptr);
}
