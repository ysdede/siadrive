#include <siaapi.h>
#include <regex>
#include <utility>
#include <filepath.h>

using namespace Sia::Api;

CSiaApi::_CSiaFileTree::_CSiaFileTree(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) :
  CSiaBase(std::move(siaDriveConfig)) {
}

CSiaApi::_CSiaFileTree::_CSiaFileTree(std::shared_ptr<CSiaDriveConfig> siaDriveConfig, const json &result) :
  CSiaBase(std::move(siaDriveConfig)) {
  if (result.find("files") != result.end()) {
    for (const auto &file : result["files"]) {
      _fileList.push_back(std::make_shared<CSiaFile>(GetSiaDriveConfig(), file));
    }
  }
}

CSiaApi::_CSiaFileTree::~_CSiaFileTree() = default;

bool CSiaApi::_CSiaFileTree::FileExists(const SString &siaPath) const {
  auto result = std::find_if(_fileList.begin(), _fileList.end(), [&](const CSiaFilePtr &item) -> bool {
    return (item->GetSiaPath() == siaPath);
  });
  return (result != _fileList.end());
}

bool CSiaApi::_CSiaFileTree::DirectoryExists(const SString &siaPath) const {
  auto result = std::find_if(_fileList.begin(), _fileList.end(), [&](const CSiaFilePtr &item) -> bool {
    return (item->GetSiaPath().BeginsWith(siaPath + "/"));
  });
  return (result != _fileList.end());
}

CSiaFilePtr CSiaApi::_CSiaFileTree::GetFile(const SString &siaPath) const {
  auto result = std::find_if(_fileList.begin(), _fileList.end(), [&](const CSiaFilePtr &item) -> bool {
    return (item->GetSiaPath() == siaPath);
  });
  return ((result != _fileList.end()) ? *result : nullptr);
}

CSiaFileCollection CSiaApi::_CSiaFileTree::Query(SString query) const {
  query = CSiaApi::FormatToSiaPath(query);
  query.Replace("*.*", "*").Replace(".", "\\.").Replace("*", "[^/]+").Replace("?", "[^/]?");
  std::regex r(query.str());
  CSiaFileCollection ret;
  std::copy_if(_fileList.begin(), _fileList.end(), std::back_inserter(ret), [&](const CSiaFilePtr &v) -> bool {
    return std::regex_match(v->GetSiaPath().str(), r);
  });
  return std::move(ret);
}

std::vector<SString> CSiaApi::_CSiaFileTree::QueryDirectories(SString rootFolder) const {
  CSiaFileCollection col;
  rootFolder.Replace("/", "\\");
  if (not rootFolder.BeginsWith('\\')) {
    rootFolder = '\\' + rootFolder;
  }
  std::vector<SString> ret;
  std::for_each(_fileList.begin(), _fileList.end(), [&](const CSiaFilePtr &v) {
    SString path = ("\\" + FilePath(v->GetSiaPath()).RemoveFileName()).Replace("/", "\\");
    if (path.BeginsWith(rootFolder)) {
      path = (rootFolder == "\\") ? path.SubString(1) : path.SubString(rootFolder.Length() + 1);
      if (path.Length()) {
        auto splitPaths = path.Split('\\');
        if (splitPaths.size()) {
          path = splitPaths[0];
          if (std::find(ret.begin(), ret.end(), path) == ret.end()) {
            ret.push_back(path);
          }
        }
      }
    }
  });
  return std::move(ret);
}
