#include "publiclistener.h"
#include <siadriveconfig.h>

using namespace Sia::Api;

CPublicListener::CPublicListener(std::shared_ptr<CSiaApi> siaApi) :
  CListenerBase(siaApi, Protocol::Http, ListenerInterface::Public, siaApi->GetSiaDriveConfig()->GetDriveApiPublicPort()) {
}

void CPublicListener::ConfigureListener(http_listener* listener) {
  listener->support(methods::GET, [this](http_request request) {

  });
  listener->support(methods::POST, [this](http_request request) {

  });
}

