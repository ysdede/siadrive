# Installation
if (MACOS)
  add_custom_command(
    TARGET siadrived
    POST_BUILD

    COMMAND ${CMAKE_COMMAND} -E copy_directory
    siadrived.app
    "siadrive.app"

    COMMAND ${CMAKE_COMMAND} -E copy
    siadrivecli.app/Contents/MacOS/siadrivecli
    "siadrive.app/Contents/MacOS/siadrivecli"


    COMMAND ${CMAKE_COMMAND} -E copy
    libsiadrive.common.api.dylib
    "siadrive.app/Contents/Frameworks/libsiadrive.common.api.dylib"

    COMMAND ${CMAKE_COMMAND} -E copy
    libsiadrive.drive.api.dylib
    "siadrive.app/Contents/Frameworks/libsiadrive.drive.api.dylib"

    COMMAND ${CMAKE_COMMAND} -E copy
    libsiadrive.fuse.api.dylib
    "siadrive.app/Contents/Frameworks/libsiadrive.fuse.api.dylib"


    COMMAND install_name_tool
    -change "@rpath/libsiadrive.common.api.dylib"
    "@executable_path/../Frameworks/libsiadrive.common.api.dylib"
    "siadrive.app/Contents/MacOS/siadrived"

    COMMAND install_name_tool
    -change "@rpath/libsiadrive.drive.api.dylib"
    "@executable_path/../Frameworks/libsiadrive.drive.api.dylib"
    "siadrive.app/Contents/MacOS/siadrived"

    COMMAND install_name_tool
    -change "@rpath/libsiadrive.fuse.api.dylib"
    "@executable_path/../Frameworks/libsiadrive.fuse.api.dylib"
    "siadrive.app/Contents/MacOS/siadrived"


    COMMAND install_name_tool
    -change "@rpath/libsiadrive.common.api.dylib"
    "@executable_path/../Frameworks/libsiadrive.common.api.dylib"
    "siadrive.app/Contents/MacOS/siadrivecli"

    COMMAND install_name_tool
    -change "@rpath/libsiadrive.drive.api.dylib"
    "@executable_path/../Frameworks/libsiadrive.drive.api.dylib"
    "siadrive.app/Contents/MacOS/siadrivecli"

    COMMAND install_name_tool
    -change "@rpath/libsiadrive.fuse.api.dylib"
    "@executable_path/../Frameworks/libsiadrive.fuse.api.dylib"
    "siadrive.app/Contents/MacOS/siadrivecli"


    COMMAND ${CMAKE_COMMAND} -E copy_directory
    ${CMAKE_CURRENT_SOURCE_DIR}/3rd_party/sia
    "siadrive.app/Contents/Resources/sia"
  )
else ()
  install(TARGETS siadrive.common.api DESTINATION ${SIADRIVE_INSTALL_FOLDER})

  install(DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}/sia DESTINATION ${SIADRIVE_INSTALL_FOLDER})
  if (MSVC)
    #install(TARGETS siadrive.dokany.api DESTINATION ${SIADRIVE_INSTALL_FOLDER})
    #install(FILES
    #  ${DOKANY_BUILD_ROOT}/x64/${CMAKE_BUILD_TYPE}/dokan1.dll
    #  ${DOKANY_BUILD_ROOT}/x64/${CMAKE_BUILD_TYPE}/dokanfuse1.dll
    #  ${DOKANY_BUILD_ROOT}/x64/${CMAKE_BUILD_TYPE}/dokannp1.dll
    #  DESTINATION ${SIADRIVE_INSTALL_FOLDER}
    #  )

    #if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    #  install(DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_BUILD_TYPE}/ DESTINATION ${SIADRIVE_INSTALL_FOLDER} FILES_MATCHING PATTERN "*.pdb" PATTERN "data" EXCLUDE PATTERN "dokany_${DOKANY_VERSION}" EXCLUDE)
    #  install(FILES ${DOKANY_BUILD_ROOT}/x64/${CMAKE_BUILD_TYPE}/dokan1.pdb DESTINATION ${SIADRIVE_INSTALL_FOLDER})
    #endif ()
  endif ()
  if (LINUX)
    install(TARGETS siadrive.fuse.api DESTINATION ${SIADRIVE_INSTALL_FOLDER})
    install(CODE "execute_process(COMMAND chmod +x ${SIADRIVE_INSTALL_FOLDER}/sia/siac)")
    install(CODE "execute_process(COMMAND chmod +x ${SIADRIVE_INSTALL_FOLDER}/sia/siad)")
  endif ()
  if (MSVC)
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/SiaDrive_Packager.iss.in ${CMAKE_CURRENT_SOURCE_DIR}/SiaDrive_Packager_${CMAKE_BUILD_TYPE}.iss @ONLY)
  endif ()
endif ()