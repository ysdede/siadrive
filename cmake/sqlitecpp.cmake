set(SQLITE_CPP_PROJECT_NAME sqlitecpp_${SQLITE_CPP_VERSION})
set(SQLITE_CPP_BUILD_ROOT ${EXTERNAL_BUILD_ROOT}/builds/${SQLITE_CPP_PROJECT_NAME})

if (UNIX)
  ExternalProject_Add(sqlitecpp_project
    DOWNLOAD_NO_PROGRESS 1
    URL https://github.com/SRombauts/SQLiteCpp/archive/${SQLITE_CPP_VERSION}.tar.gz
    PREFIX ${SQLITE_CPP_BUILD_ROOT}
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${EXTERNAL_BUILD_TYPE} -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DSQLITECPP_RUN_CPPLINT=OFF -DSQLITECPP_RUN_CPPCHECK=OFF
    INSTALL_COMMAND ${CMAKE_COMMAND} -E echo "Skipping install step.")
else()
  ExternalProject_Add(sqlitecpp_project
    DOWNLOAD_NO_PROGRESS 1
    URL https://github.com/SRombauts/SQLiteCpp/archive/${SQLITE_CPP_VERSION}.tar.gz
    PREFIX ${SQLITE_CPP_BUILD_ROOT}
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${EXTERNAL_BUILD_TYPE} -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DSQLITECPP_RUN_CPPLINT=OFF -DSQLITECPP_RUN_CPPCHECK=OFF
    INSTALL_COMMAND ${CMAKE_COMMAND} -E echo "Skipping install step.")
endif()

if (MSVC)
  if (CMAKE_GENERATOR_LOWER STREQUAL "nmake makefiles")
    set(SQLITE_CPP_LIBRARIES
      ${SQLITE_CPP_BUILD_ROOT}/src/sqlitecpp_project-build/sqlite3/sqlite3.lib
      ${SQLITE_CPP_BUILD_ROOT}/src/sqlitecpp_project-build/SQLiteCpp.lib
      )
  else ()
    set(SQLITE_CPP_LIBRARIES
      ${SQLITE_CPP_BUILD_ROOT}/src/sqlitecpp_project-build/sqlite3/${CMAKE_BUILD_TYPE}/sqlite3.lib
      ${SQLITE_CPP_BUILD_ROOT}/src/sqlitecpp_project-build/${CMAKE_BUILD_TYPE}/SQLiteCpp.lib
      )
  endif ()
endif ()
if (UNIX)
  set(SQLITE_CPP_LIBRARIES
    ${SQLITE_CPP_BUILD_ROOT}/src/sqlitecpp_project-build/sqlite3/libsqlite3.a
    ${SQLITE_CPP_BUILD_ROOT}/src/sqlitecpp_project-build/libSQLiteCpp.a
    )
endif ()

set(SQLITE_CPP_INCLUDE_DIRS
  ${SQLITE_CPP_BUILD_ROOT}/src/sqlitecpp_project/sqlite3
  ${SQLITE_CPP_BUILD_ROOT}/src/sqlitecpp_project/include
  )
set(SIADRIVE_SYSTEM_INCLUDES ${SQLITE_CPP_INCLUDE_DIRS} ${SIADRIVE_SYSTEM_INCLUDES})